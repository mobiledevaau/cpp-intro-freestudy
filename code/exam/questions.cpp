#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <cassert>

int sum(int a, int b)
{
    return a + b;
}

struct shape
{
    virtual double area() = 0;
};

struct square : public shape
{
    square(int side_length) : m_side_length(side_length)
    { }

    square() : m_side_length(0)
    { }

    double area()
    {
        return m_side_length * m_side_length;
    }

    int m_side_length;
};

int main()
{
    // Use at least 5 different modern C++11/14 features to update the following code

    // Question: fill the vector with values 1-10
    std::vector<int> v;
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);
    v.push_back(4);
    v.push_back(5);
    v.push_back(6);
    v.push_back(7);
    v.push_back(8);
    v.push_back(9);
    v.push_back(10);

    // Question: Initialize mymap
    std::map<char,int> mymap;

    mymap['b'] = 100;
    mymap['a'] = 200;
    mymap['c'] = 300;

    // Question: show content of mymap:
    for (std::map<char,int>::iterator it = mymap.begin(); it != mymap.end(); ++it)
    {
        std::cout << it->first << " => " << it->second << std::endl;
    }

    // Question: Use smart_pointers to handle resource allocation
    std::string *course = new std::string("C++11/14 free study activity");

    if (course)
    {
        delete course;
        course = 0;
    }

    // Question: Calculate value at compile time
    int number = sum(2,3);

    // Question: Make sure a short is 2 bytes at compile time
    assert(sizeof(short) == 2);

    // Question: Make sure we override area and initialize m_side_length
    square s(3);

    // Question: Call sum using a std::function
    int mysum = sum(3,4);

    return 0;
}
