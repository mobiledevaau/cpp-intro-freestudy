# Overview

Files we will use in lecture 2 of the free study activity:

* values_pointers_references.cpp
    * Shows the difference between values, pointers and references

* raw_string.cpp
    * Shows the use of raw string litterals

* constexpr_factorial.cpp
    * The constexpr factorial function we measure in the lecture
