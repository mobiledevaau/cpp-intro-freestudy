#include <iostream>

class our_object
{
public:
    // some stuff
};

// We have to choose between pass by value and reference
// void f(our_object v)
// {
//     // pass by value
// }

void f(our_object* v)
{
    // pass by pointer
}

void f(our_object& v)
{
    // pass by reference
}

void f(const our_object& v)
{
    // pass by const reference
}


int main()
{
    our_object o; // a value
    our_object* p = &o; // a pointer
    our_object& r = o; // a reference
    const our_object& cr = o; // a const reference

    f(o);
    f(p);
    f(r);
    f(cr);

    // These to variants of references are called l-value references

    // Key difference between pointers and references (there are more):
    //
    //    o A reference has to be bound to a value
    //
    //    o A reference cannot be changed to refer to a different object
    //
    //    o Pointers support iteration ++/-- etc.
    //
    //    o You have to de-reference a point to access the memory location
    //      it points to.
    //
    //    o Const references can be bound to temporaries. Pointers cannot
    //      (without some redirection)

    return 0;
}
